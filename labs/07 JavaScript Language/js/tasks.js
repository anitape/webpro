// tasks.js
// This script manages a to-do list.

// Need a global variable:
var tasks = []; 

// Function called when the form is submitted.
// Function adds a task to the global array.
function addTask() {
    'use strict';

    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');
    
    // For the output:
    var message = '';

    if (task.value) {
    
        // Add the item to the array:
        tasks.push(task.value);
        
        // Update the page:
        message = '<h2>To-Do</h2><ol>';
        for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;
        
    } // End of task.value IF.

    // Return false to prevent submission:
    return false;
    
} // End of addTask() function.

function removeDuplicates() {
    'use strict';
    var output = document.getElementById('output');
    var count = tasks.length;
    var out=[];
    var obj={};

    for (var i = 0;  i < count; i++) {
        obj[tasks[i]] = 0;
    }
    for (i in obj) {
        out.push(i);
    }

   var message = '<h2>To-Do</h2><ol>';
    for (var x = 0; x < out.length; x++) {
        message += '<li>' + out[x] + '</li>';
    }
    message += '</ol>';
    output.innerHTML = message;
    return false;
}

// Initial setup:
function init() {
    // 'use strict';
    document.getElementById('submit').onclick = addTask;
    document.getElementById('remove').onclick = removeDuplicates;
} // End of init() function.


window.onload = init;