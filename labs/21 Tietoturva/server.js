var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static('public'));
app.get('/index.htm', function (req, res) {
    res.sendFile( __dirname + "/" + "index.htm" );
});

const { check, validationResult } = require('express-validator');

app.post('/process_post', urlencodedParser,

    [check('first_name').isLength({ min: 2 }).withMessage("vähintään kaksi merkkiä!"),
        check('last_name').isLength({ min: 2 }).withMessage("vähintään kaksi merkkiä!"),
        check('age').isInt({ min: 0 }).withMessage("pitää olla numeroita ja vähintään 0"),
        check('email').isEmail({require_tld: true}).withMessage("Oikea email-muoto!")],
        function (req, res) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({errors: errors.array()});
            }
            else {
                response = {
                    first_name:req.body.first_name,
                    last_name:req.body.last_name,
                    age:req.body.age,
                    email:req.body.email
                };
                console.log(response);
                res.end(JSON.stringify(response));
            }

});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port)
});