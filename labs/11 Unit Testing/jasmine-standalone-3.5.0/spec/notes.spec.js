var notes = (function() {
    var list = [];

    return {
        add: function(note) {
            if (note) {
                var item = {timestamp: Date.now(), text: note};
                if (note.trim().length) {
                    list.push(item);
                    return true;
                }
            }
            return false;
        },
        remove: function(index) {
            if (index < list.length) {
                list.splice(index, 1);
                return true;
            }
            return false;
        },
        count: function() {
            return list.length;
        },
        list: function() {
            return list;
        },
        find: function(str) {
            if (str) {
                var newStr = str.toLowerCase();
                var newList = [];
                for (var i = 0; i < list.length; i++) {
                    if (list[i].text.includes(newStr)) {
                        //console.log(list[i].text);
                        newList.push(list[i].text);
                    }
                }
                if (newList.length === 0) {
                    return false;
                }
                //console.log(newList);
                return newList;
            }
            return false;
        },
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());

describe('notes module', function () {
    beforeEach(function() {
        notes.clear();
        notes.add('first note');
        notes.add('second note');
        notes.add('third note');
        notes.add('fourth note');
        notes.add('fifth note');
    });

    describe('adding a note', function() {
        it('should be able to add a new note', function () {
            expect(notes.add('sixth note')).toBe(true);
            expect(notes.count()).toBe(6);
        });
        it('should ignore blank notes', function () {
            expect(notes.add('')).toBe(false);
            expect(notes.count()).toBe(5);
        });
        it('should ignore notes containing only whitespace', function() {
            expect(notes.add('   ')).toBe(false);
            expect(notes.count()).toBe(5);
        });
        it('should require a string parameter', function() {
            expect(notes.add()).toBe(false);
            expect(notes.count()).toBe(5);
        });

        describe('deleting a note', function() {
            it('should be able to delete the first index', function() {
                expect(notes.remove(0)).toBe(true);
                expect(notes.count()).toBe(4);
            });
            it('should be able to delete the last index', function() {
                expect(notes.remove(4)).toBe(true);
                expect(notes.count()).toBe(4);
            });
            it('should return false if index is out of range', function() {
                expect(notes.remove(5)).toBe(false);
                expect(notes.count()).toBe(5);
            });
            it('should return false if the parameter is missing', function() {
                expect(notes.remove()).toBe(false);
                expect(notes.count()).toBe(5);
            });

            describe('finding a note', function() {
                it('should be able to search a note by string "note" ' +
                    'and return an array containing all notes that contain the specified string', function() {
                    expect(notes.find('note')).toEqual(['first note','second note','third note','fourth note','fifth note']);
                });
                it('should be able to search a note by string "Note" ' +
                    'and return an array containing all notes that contain the specified string. ' +
                    'Search is not case sensitive.', function() {
                    expect(notes.find('Note')).toEqual([ 'first note', 'second note', 'third note', 'fourth note', 'fifth note' ]);
                });
                it('should be able to search a note by string "th" ' +
                    'and return an array containing all notes that contain the specified string', function() {
                    expect(notes.find('th')).toEqual(['third note','fourth note','fifth note']);
                });
                it('should be able to search a note by string "four" ' +
                    'and return an array containing all notes that contain the specified string', function() {
                    expect(notes.find('four')).toEqual(['fourth note']);
                });
                it('should return false when try to find a note by string "six", ' +
                    'because no matches will be found', function () {
                    expect(notes.find('six')).toBe(false);
                });
                it('should return false when try to find a note by blank string', function() {
                    expect(notes.find('')).toBe(false);
                });
                it('should search without passing a parameter and return false', function() {
                    expect(notes.find()).toBe(false);
                });
            });

        });

    });

});