
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

var list = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title + ", " + books[i].year);
	var item = document.createElement('li');
	item.innerHTML = books[i].title + ", " + books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);

var table = document.createElement('table');

for (var i=0; i < books.length; i++) {
	if (i === 0 ) {
		var hrow = document.createElement('tr');
		var hcell1 = document.createElement('th');
		hcell1.innerHTML = 'Title';
		var hcell2 = document.createElement('th');
		hcell2.innerHTML = 'Year';
		hrow.appendChild(hcell1);
		hrow.appendChild(hcell2);
		table.appendChild(hrow);
	}
	var row = document.createElement('tr');
	var cell1 = document.createElement('td');
	cell1.innerHTML = books[i].title;
	var cell2 = document.createElement('td');
	cell2.innerHTML = books[i].year;
	row.appendChild(cell1);
	row.appendChild(cell2);
	table.appendChild(row);
}

document.body.appendChild(table);

window.onload = function () {
	console.log('Loaded');
	var heading = document.querySelector('h1');
	//var title = document.querySelector('td').value;
	//console.log(title);
	//heading.innerHTML = title;

	var table = document.querySelector('table');
	var rows = table.getElementsByTagName("tr");
	for (var i = 1; i < rows.length; i++) {
		var curRow = table.rows[i];
		//get cell data from first col of row
		curRow.onclick = function () {
			console.log('Clicked!');
			var cell = this.getElementsByTagName("td")[0].innerHTML;
			console.log(cell);
			heading.innerHTML = cell;
		};
	}
};